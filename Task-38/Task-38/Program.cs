﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            int userInput = 0;

            Console.WriteLine("Devision Table\n");
            Console.Write("Your number: ");
            userInput = int.Parse(Console.ReadLine());

            for (int i = 1; i <= 12; i++)
            {
                int count = userInput*i;

                Console.WriteLine($"{count} / {userInput} = {count / userInput}");
            }
        }
    }
}
