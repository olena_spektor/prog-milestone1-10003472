﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 5;

            for (int i = 0; i <= count; i++)
            {
                if (i == count)
                {
                    Console.WriteLine($"\nThe index {i} is equal to the counter {count}.\n");
                }
                else
                {
                    Console.WriteLine($"The index {i} isn't equal to the counter {count}.\n");
                }
            }
            Console.ReadLine();
        }
    }
}
