﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            int option;

            Console.WriteLine("1. Convert Celsius to Fahrenheit.");
            Console.WriteLine("2. Convert Fahrenheit to Celsius.");
            Console.WriteLine();
            Console.Write("Please choose an option (only number):  ");
            option = int.Parse(Console.ReadLine());

            var celsius = 0;
            var fahrenheit = 0;
            double f_temp;
            double c_temp;

            switch (option)
            {
                case 1:
                    Console.Clear();
                    Console.Write("How many Celsius do you want to convert? ");
                    celsius = int.Parse(Console.ReadLine());

                    f_temp = celsius * 1.8 + 32; // To convert C to F use formula -  Temp(F) = Temp(C) * 1.8 + 32

                    Console.WriteLine();
                    Console.WriteLine($"{celsius} C = {f_temp} F");
                    Console.ReadLine();
                    break;

                case 2:
                    Console.Clear();
                    Console.WriteLine("How many Fahrenheit do you want to convert? ");
                    fahrenheit = int.Parse(Console.ReadLine());

                    c_temp = (fahrenheit - 32) / 1.8; // To convert F to C use formula - Temp(C) = (Temp(F) - 32) / 1.8

                    Console.WriteLine();
                    Console.WriteLine($"{fahrenheit} F = {c_temp} C");
                    Console.ReadLine();
                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Sorry, the option does not exist.");
                    Console.ReadLine();
                    break;

            }
        }
    }
}

