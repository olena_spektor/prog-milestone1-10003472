﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();
            names.Add(Tuple.Create("Julie", 16));
            names.Add(Tuple.Create("Jack", 18));
            names.Add(Tuple.Create("Ben", 20));

            Console.WriteLine($"1. {names[0].Item1}, {names[0].Item2}");
            Console.WriteLine($"2. {names[1].Item1}, {names[1].Item2}");
            Console.WriteLine($"3. {names[2].Item1}, {names[2].Item2}");
            Console.ReadLine();
        }
    }
}
