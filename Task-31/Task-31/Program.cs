﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 0;
            

            Console.Write("Please a number:     ");
            number = int.Parse(Console.ReadLine());

            if (number % 3 == 0)
            {
                Console.WriteLine();
                Console.WriteLine($"The number {number} devisible by 3");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine($"The number {number} isn't devisible by 3");
            }

            if (number % 4 == 0)
            {
                Console.WriteLine();
                Console.WriteLine($"The number {number} is devisible by 4.");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine($"The number {number} isn't devisible by 4.");
            }

            Console.ReadLine();
        }
    }
}
