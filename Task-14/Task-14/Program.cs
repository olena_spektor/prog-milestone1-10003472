﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            int terabyte = 0;

            Console.Write("How many terabytes does your computer storage has? ");
            terabyte = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine($"Your computer storage is {terabyte} TB, which is {terabyte*1024} GB");
            Console.ReadLine();            
        }
    }
}
