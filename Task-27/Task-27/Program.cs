﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] color = new string[5] {"Red", "Blue", "Yellow", "Green", "Pink"};

            Array.Sort(color);

            Console.WriteLine(string.Join(", ", color));
            Console.ReadLine();
        }
    }
}
