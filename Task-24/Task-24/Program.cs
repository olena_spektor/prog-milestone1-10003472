﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var option = 0;
            char menu;

            do
            {
                Console.WriteLine("******************************************");
                Console.WriteLine("*                                        *");
                Console.WriteLine("*    1. Read a joke #1                   *");
                Console.WriteLine("*    2. Read a joke #2                   *");
                Console.WriteLine("*    3. Read a joke #3                   *");
                Console.WriteLine("*    4. Exit                             *");
                Console.WriteLine("*                                        *");
                Console.WriteLine("******************************************");
                Console.WriteLine();

                Console.Write("Please choose an option:  ");
                option = int.Parse(Console.ReadLine());


                if (option <= 0 || option >= 5)
                {
                    do
                    {
                        Console.WriteLine("");
                        Console.WriteLine("Please, choose the correct option.");
                        option = int.Parse(Console.ReadLine());
                    } while (option <= 0 || option >= 5);
                }


                if (option == 1)
                {
                    Console.Clear();
                    Console.WriteLine("************************************************************************************");
                    Console.WriteLine();
                    Console.WriteLine("A young man was walking along in the forest,");
                    Console.WriteLine("when he heard a muffled voice crying for help from behind a log.");
                    Console.WriteLine("He leaned over to see a frog sitting in the mud.");
                    Console.WriteLine("The frog looked up at him and said,");
                    Console.WriteLine();
                    Console.WriteLine("     I'm actually a beautiful princess, and if you kiss me,");
                    Console.WriteLine("     I'll transform back into my true self, and be yours for eternity.");
                    Console.WriteLine();
                    Console.WriteLine("Silently, the man scooped up the frog and continued on his walk.");
                    Console.WriteLine("A minute or two later, the frog piped up again,");
                    Console.WriteLine();
                    Console.WriteLine("     Hey, buddy, maybe you didn't hear me -- ");
                    Console.WriteLine("     I said, if you kiss me, I'll turn into a princess. What are you waiting for?");
                    Console.WriteLine();
                    Console.WriteLine("Annoyed, the man stuffed the frog into his coat pocket.");
                    Console.WriteLine("Shocked, the frog yelled from inside the man's pocket,");
                    Console.WriteLine();
                    Console.WriteLine("     What the hell?  I'm a princess!All you have to do is kiss me!");
                    Console.WriteLine();
                    Console.WriteLine("Opening his pocket and peering in, the man said,");
                    Console.WriteLine();
                    Console.WriteLine("     Listen -- I'm a software engineer.  I don't have time for a girlfriend.");
                    Console.WriteLine("     But a talking frog is kind of cool.");
                    Console.WriteLine();
                    Console.WriteLine("************************************************************************************");
                    Console.WriteLine();

                    Console.WriteLine("Go to Menu - press <m> or <M>");
                    menu = char.Parse(Console.ReadLine());

                    if (menu == 'm' || menu == 'M')
                    {
                        Console.Clear();
                        continue;
                    }

                }

                if (option == 2)
                {

                    Console.Clear();
                    Console.WriteLine("************************************************************************************");
                    Console.WriteLine();
                    Console.WriteLine("Company: “[Company] tech support, how may I help you?”");
                    Console.WriteLine();
                    Console.WriteLine("Caller: “Hi, I’ve got a problem. Your program is telling me to get a pet snake.");
                    Console.WriteLine("I don’t want one.”");
                    Console.WriteLine();
                    Console.WriteLine("Company: “Excuse me?”");
                    Console.WriteLine();
                    Console.WriteLine("Caller: “It’s giving me a message telling me I need a snake to run it.”");
                    Console.WriteLine();
                    Console.WriteLine("Company: “Read the message to me please.”");
                    Console.WriteLine();
                    Console.WriteLine("Caller: “Error: Python required to run script.”");
                    Console.WriteLine();
                    Console.WriteLine("************************************************************************************");
                    Console.WriteLine();

                    Console.WriteLine("Go to Menu - press <m> or <M>");
                    menu = char.Parse(Console.ReadLine());

                    if (menu == 'm' || menu == 'M')
                    {
                        Console.Clear();
                        continue;
                    }

                }

                if (option == 3)
                {
                    Console.Clear();
                    Console.WriteLine("************************************************************************************");
                    Console.WriteLine();
                    Console.WriteLine("A wife calls her programmer husband and tells him,");
                    Console.WriteLine("     'While you're out, buy some milk.'");
                    Console.WriteLine();
                    Console.WriteLine("He never returns home.");
                    Console.WriteLine();
                    Console.WriteLine("************************************************************************************");
                    Console.WriteLine();


                    Console.WriteLine("Go to Menu - press <m> or <M>");
                    menu = char.Parse(Console.ReadLine());

                    if (menu == 'm' || menu == 'M')
                    {
                        Console.Clear();
                        continue;
                    }

                }

                if (option == 4)
                {
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine("****************************");
                    Console.WriteLine("**                        **");
                    Console.WriteLine("**    See you later!      **");
                    Console.WriteLine("**                        **");
                    Console.WriteLine("**    Press <ENTER>       **");
                    Console.WriteLine("**                        **");
                    Console.WriteLine("****************************");
                    Console.ReadLine();

                    break;

                }

            } while (true);
        }
    }
}