﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = "";
            var day = "";

            Console.WriteLine("*********************************************************");
            Console.Write("What month were you born? ");
            month = Console.ReadLine();

            Console.WriteLine();
            Console.Write("What day were you born? (Please type just a number) ");
            day = Console.ReadLine();

            Console.WriteLine("*********************************************************");
            Console.WriteLine();
            Console.WriteLine($"You were born on the {day} of {month}.");
            Console.ReadLine();
        }
    }
}
