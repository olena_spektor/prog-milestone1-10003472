﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] dayOFweek = new string[7] {"Monday", "Tuesday", "Wednesday", "Thurthday", "Friday", "Saturday", "Sunday"};
            int number = 1;

            
            Console.WriteLine("{0} is {1}st day of the week.\n", dayOFweek[0], number++);
            Console.WriteLine("{0} is {1}nd day of the week.\n", dayOFweek[1], number++);
            Console.WriteLine("{0} is {1}rd day of the week.\n", dayOFweek[2], number++);
            Console.WriteLine("{0} is {1}th day of the week.\n", dayOFweek[3], number++);
            Console.WriteLine("{0} is {1}th day of the week.\n", dayOFweek[4], number++);
            Console.WriteLine("{0} is {1}th day of the week.\n", dayOFweek[5], number++);
            Console.WriteLine("{0} is {1}th day of the week.\n", dayOFweek[6], number++);
            Console.ReadLine();
        }
    }
}
