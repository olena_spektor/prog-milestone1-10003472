﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 0.0;
            var b = 0.0;
            var c = 0.0;
            var sum = 0.0;
            var ans = true;
            var gstPersent= 15.00;
            var gst = 0.0;

            Console.WriteLine("**************************************************************************");
            Console.WriteLine("How much does your 1st item cost (float-pointing number)?");
            a = double.Parse(Console.ReadLine());

            Console.WriteLine("How much does your 2nd item cost (float-pointing number)?");
            b = double.Parse(Console.ReadLine());

            Console.WriteLine("How much does your 3rd item cost (float-pointong number)?");
            c = double.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("**************************************************************************");
            Console.WriteLine();
            Console.Write($"You have entered ${a}, ${b}, ${c}.");
            Console.WriteLine();

            sum = a + b + c;

            Console.WriteLine("Is GST already included in the price? (true/ false) ");
            ans = bool.Parse(Console.ReadLine());
            Console.WriteLine();

            if(ans)
            {
                Console.WriteLine($"The total price is ${sum} including GST(15%).");
                Console.ReadLine();
            }
            else
            {
                gst = sum * gstPersent / 100;
                Console.WriteLine($"The total price is ${sum + gst} including GST (15%).");
                Console.ReadLine();
            }


        }
    }
}
