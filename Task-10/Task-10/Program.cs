﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int leapYears;
            int currentYear = 2016;
            const int loop = 4; // leap year is every fourth year
            int futureTime = 20;

            leapYears = ((currentYear % loop) - futureTime) /-loop;

            Console.WriteLine($"There are {leapYears} leap years in the next {futureTime} years.");
            Console.ReadLine();
        }
    }
}
