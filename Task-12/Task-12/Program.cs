﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 0;

            Console.WriteLine("Please enter the number: ");
            number = int.Parse(Console.ReadLine());

            if ((number % 2) == 0)
            {
                Console.WriteLine($"{number} is an even number.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine($"{number} is an odd number.");
                Console.ReadLine();
            }
        }
    }
}
