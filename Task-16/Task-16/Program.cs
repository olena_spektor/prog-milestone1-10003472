﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            var words = "Hello12";
            int number = 0;

            for (int i = 0; i < words.Length; i++)
            {
                if (char.IsLetter(words[i]))
                {
                    number++;                    
                }                           
            }
            Console.WriteLine($"The word is {words}.");
            Console.WriteLine($"The amount of letters in the word is {number}.");
            Console.WriteLine($"The length of the word is {words.Length}.");
            Console.ReadLine();
        }
    }
}
