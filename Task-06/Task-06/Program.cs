﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int ans = 0;

            for (int i = 1; i <= 5; i++)
            {
                Console.WriteLine("Type your first number: ");
                a = int.Parse(Console.ReadLine());

                ans += a;
            }
            Console.WriteLine();
            Console.WriteLine($"The total is {ans}.");
            Console.ReadLine();
        }
    }
}
