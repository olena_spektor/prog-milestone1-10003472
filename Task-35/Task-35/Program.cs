﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int b = 0;
            int c = 0;

            Console.Write("What's your 1st number (number 'a')?\t");
            a = int.Parse(Console.ReadLine());
            Console.Write("\nWhat's your 2nd number (number 'b')?\t");
            b = int.Parse(Console.ReadLine());
            Console.Write("\nWhat's your 3rd number (number 'c')?\t");
            c = int.Parse(Console.ReadLine());

            int option = 0;
            char ans = 'y';

            do
            {
                Console.Clear();
                Console.WriteLine($"Your numbers are: \nnumber 'a' = {a}\nnumber 'b' = {b}\nnumber 'c' = {c}");
                Console.WriteLine("\n*****************************************************************\n");

                Console.WriteLine("1. a + b + c");
                Console.WriteLine("2. a * b * c");
                Console.WriteLine("3. a * b - c");
                Console.WriteLine("4. (a + b) / c");
                Console.WriteLine("5. (a + b) * c");

                Console.Write("\nChoose what you want to do with your numbers:\t");
                option = int.Parse(Console.ReadLine());

                if (option == 1)
                {
                    Console.Clear();
                    Console.WriteLine($"The answer is {a + b + c}.\n");

                    Console.Write("Do you want to calculate once more (<y/n>)? ");
                    ans = char.Parse(Console.ReadLine());

                    if (ans == 'y')
                    {
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("\nSee you later!\n");
                        break;
                    }

                }
                else if (option == 2)
                {
                    Console.Clear();
                    Console.WriteLine($"The answer is {a * b * c}.\n");

                    Console.Write("Do you want to calculate once more (<y/n>)? ");
                    ans = char.Parse(Console.ReadLine());

                    if (ans == 'y')
                    {
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("\nSee you later!\n");
                        break;
                    }
                }
                else if (option == 3)
                {
                    Console.Clear();
                    Console.WriteLine($"The answer is {a * b - c}.\n");

                    Console.Write("Do you want to calculate once more (<y/n>)? ");
                    ans = char.Parse(Console.ReadLine());

                    if (ans == 'y')
                    {
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("\nSee you later!\n");
                        break;
                    }
                }
                else if (option == 4)
                {
                    Console.Clear();
                    Console.WriteLine($"The answer is {(a + b) / c}.\n");

                    Console.Write("Do you want to calculate once more (<y/n>)? ");
                    ans = char.Parse(Console.ReadLine());

                    if (ans == 'y')
                    {
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("\nSee you later!\n");
                        break;
                    }
                }
                else if (option == 5)
                {
                    Console.Clear();
                    Console.WriteLine($"The answer is {(a + b) * c}.\n");

                    Console.Write("Do you want to calculate once more (<y/n>)? ");
                    ans = char.Parse(Console.ReadLine());

                    if (ans == 'y')
                    {
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("\nSee you later!\n");
                        break;
                    }
                }

            } while (ans != 'n');


        }
    }
}
