﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> calendar = new Dictionary<string, int>();

            calendar.Add("January", 31);
            calendar.Add("Februrary", 28);
            calendar.Add("March", 31);
            calendar.Add("April", 30);
            calendar.Add("May", 31);
            calendar.Add("June", 30);
            calendar.Add("July", 31);
            calendar.Add("August", 31);
            calendar.Add("September", 30);
            calendar.Add("October", 31);
            calendar.Add("November", 30);
            calendar.Add("December", 31);          
               
            foreach (KeyValuePair<string, int> pair in calendar)
            {
                if (pair.Value == 31)
                {
                    Console.WriteLine($"{pair.Key} {pair.Value}");
                }                    
            }                                                 
        }
    }
}
